import argparse

import gym
import tensorflow as tf
from loguru import logger
from stable_baselines import SAC
import yaml

import gym_donkeycar
from src.utility import seed, load_vae
from src.wrappers import make_wrappers
from src.command import common_args, parse_args

tf.logging.set_verbosity(tf.logging.ERROR)


def main(args: dict):
    vae = load_vae(args["vae_path"])

    env = gym.make(args["environment_id"], start_delay=0)
    try:
        env = make_wrappers(env, vae)

        env.metadata["video.frames_per_second"] = 10
        env = gym.wrappers.monitor.Monitor(
            env,
            directory=args["monitoring_dir"],
            force=True,
            video_callable=lambda episode: episode % 5,  # Dump every 5 episodes
        )

        with open("hyperparams/sac.yaml", "r") as f:
            hyperparams_dict = yaml.safe_load(f)
            hyperparams = hyperparams_dict[args["environment_id"]]
            logger.debug(f"Policy hyperparameters: {hyperparams}")

        model = SAC(
            "MlpPolicy",
            env,
            tensorboard_log=str(args["tensorboard_dir"]),
            verbose=1,
            learning_starts=0,  # default 100
            seed=42,
            **hyperparams,
        )

        seed(42, env)

        logger.info(f"Learning. CTRL+C to quit.")
        model.learn(total_timesteps=50000, log_interval=1)
    finally:
        logger.info(f'Saving model to {args["model_path"]}, don\'t quit!')
        model.save(args["model_path"])
        env.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Train the SAC algorithm on the DonkeyCar environment with a VAE."
    )
    parser = common_args(parser)
    main(parse_args(parser))
