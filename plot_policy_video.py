import argparse
import logging
from pathlib import Path

# Disable all those horrible TF warning messages
logging.getLogger("tensorflow").disabled = True

import gym
from gym.wrappers.monitor import Monitor
import gym_donkeycar
from loguru import logger
from stable_baselines import SAC
import tensorflow as tf

from src.utility import seed, load_vae
from src.wrappers import make_wrappers
from src.command import common_args, parse_args

tf.logging.set_verbosity(tf.logging.ERROR)


def main(args: dict):
    vae = load_vae(args["vae_path"])

    env = gym.make(args["environment_id"], start_delay=0)
    try:
        env = make_wrappers(env, vae)

        env.metadata["video.frames_per_second"] = 10
        env = gym.wrappers.monitor.Monitor(
            env,
            directory=args["monitoring_dir"],
            force=True,
            video_callable=lambda x: True,  # Dump every episode
        )

        model = SAC.load(args["model_path"])

        seed(42, env)
        obs = env.reset()
        for _ in range(args["max_time_steps"]):
            action = model.predict(obs, deterministic=True)[0]
            obs, _, done, _ = env.step(action)
            if done:
                break
    finally:
        env.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a video of a policy")
    parser = common_args(parser)
    parser.add_argument(
        "--max-time-steps",
        type=int,
        default=500,
        help="Maximum number of timesteps to run simulation for.",
    )
    main(parse_args(parser))
