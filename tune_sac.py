# TODO: I haven't cleaned this up yet. Didn't improve results much so didn't invest time in it.

import glob
import os
import time
import uuid
from pathlib import Path

import gym
from loguru import logger
from stable_baselines import SAC
from stable_baselines.bench import Monitor
from stable_baselines.common.vec_env import DummyVecEnv

import gym_donkeycar
from src.tune.hyperparameters_opt import hyperparam_optimization
from src.utility import load_vae
from src.wrappers import make_wrappers

tensorboard_log = "log"
seed = 42
vae = load_vae("pretrained-models/vae/vae-donkey-generated-roads-v0-32.pkl")
env_kwargs = {}


def get_latest_run_id(log_path, env_id):
    """
    Returns the latest run number for the given log name and log path,
    by finding the greatest number in the directories.
    :param log_path: (str) path to log folder
    :param env_id: (str)
    :return: (int) latest run number
    """
    max_run_id = 0
    for path in glob.glob(log_path + "/{}_[0-9]*".format(env_id)):
        file_name = path.split("/")[-1]
        ext = file_name.split("_")[-1]
        if (
            env_id == "_".join(file_name.split("_")[:-1])
            and ext.isdigit()
            and int(ext) > max_run_id
        ):
            max_run_id = int(ext)
    return max_run_id


log_folder = "log_sac"
algo = "sac"
env_id = "donkey-generated-roads-v0"
uuid_str = "_{}".format(uuid.uuid4())

log_path = "{}/{}/".format(log_folder, algo)
save_path = os.path.join(
    log_path,
    "{}_{}{}".format(env_id, get_latest_run_id(log_path, env_id) + 1, uuid_str),
)
params_path = "{}/{}".format(save_path, env_id)
os.makedirs(params_path, exist_ok=True)


def make_env(env_id, rank=0, seed=0, log_dir=None, wrapper_class=None, env_kwargs=None):
    if log_dir is not None:
        os.makedirs(log_dir, exist_ok=True)

    if env_kwargs is None:
        env_kwargs = {}

    def _init():
        p = 9091 + rank
        logger.info(f"Connecting to donkey_sim on port {p}")
        env = gym.make(env_id, port=p, start_delay=10.0, **env_kwargs)
        # Add standard wrappers
        env = make_wrappers(env, vae)
        env.seed(seed + rank)
        log_file = os.path.join(log_dir, str(rank)) if log_dir is not None else None
        env = Monitor(env, log_file)
        return env

    return _init


def create_env(n_envs, eval_env=False):
    """
    Create the environment and wrap it if necessary
    :param n_envs: (int)
    :param eval_env: (bool) Whether is it an environment used for evaluation or not
    :return: (Union[gym.Env, VecEnv])
    :return: (gym.Env)
    """
    global hyperparams
    global env_kwargs

    # Do not log eval env (issue with writing the same file)
    log_dir = None if eval_env else save_path

    # env = SubprocVecEnv([make_env(env_id, i, args.seed) for i in range(n_envs)])
    # On most env, SubprocVecEnv does not help and is quite memory hungry
    env = DummyVecEnv(
        [
            make_env(
                env_id,
                100 + i if eval_env else i,
                seed,
                log_dir=log_dir,
                env_kwargs=env_kwargs,
            )
            for i in range(n_envs)
        ]
    )
    return env


def create_model(*_args, **kwargs):
    return SAC(
        "MlpPolicy",
        env=create_env(1),
        tensorboard_log=os.path.join(tensorboard_log, env_id),
        verbose=1,
        learning_starts=0,  # default 100
        **kwargs,
    )


data_frame = hyperparam_optimization(
    "sac",
    create_model,
    create_env,
    n_trials=100,
    n_timesteps=5000,  # Same as model.learn(total_timesteps)
    hyperparams=None,
    n_jobs=1,
    seed=42,
    sampler_method="tpe",
    pruner_method="median",
)

p = Path(log_folder) / "report_{}_{}.csv".format(env_id, int(time.time()))
p.parent.mkdir(parents=True, exist_ok=True)
logger.info("Writing report to {}".format(p))
data_frame.to_csv(str(p))
